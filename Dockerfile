FROM ubuntu:latest

MAINTAINER pbrlod

# Install apache, PHP 7, and supplimentary programs. openssh-server, curl, and lynx-cur are for debugging the container.
RUN apt-get update && \
    apt-get -y upgrade && \
    apt-get -y install \
    apache2 \
    libxml2-dev \
    libxslt-dev \
    python-pip \
    zlib1g-dev \
    libgdbm-dev \
    python-levenshtein \
    python-simplejson \
    python-lxml \
#    wget \
    vim \
    tar \
    sudo


RUN mkdir /tmp/Philologic
ADD https://github.com/ARTFL-Project/PhiloLogic4/archive/v4.5.9.tar.gz /tmp/
#RUN /usr/bin/wget -P /tmp/Philologic https://github.com/ARTFL-Project/PhiloLogic4/archive/v4.5.9.tar.gz
RUN /bin/tar -xvzf /tmp/v4.5.9.tar.gz -C /tmp/
RUN a2enmod rewrite
RUN a2enmod cgi

RUN cd /tmp/PhiloLogic4-4.5.9 && bash /tmp/PhiloLogic4-4.5.9/install.sh
RUN /bin/cp -rv /var/lib/philologic4/web_app/* /var/www/html/
COPY epigraphie.sh /root/
COPY hagiographie.sh /root/
COPY cbma.sh /root/
COPY start.sh /root/
COPY philologic4.cfg /etc/philologic/
COPY 000-default.conf /etc/apache2/sites-enabled/000-default.conf
RUN chown -R www-data:www-data /var/www/html
RUN /usr/sbin/update-rc.d apache2 defaults  

#volume
VOLUME ["/media"]

# Expose apache.
EXPOSE 80

# By default start up apache in the foreground, override with /bin/bash for interative
CMD /usr/sbin/apache2ctl -D FOREGROUND
